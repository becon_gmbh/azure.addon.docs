##################
Welcome to the Azure AddOn manuals
##################

The Azure AddOn serves as an interface between i-doit and Azure. Azure synchronizes the virtual systems into the leading i-doit system.

------------

This documentation is organized into a couple of sections:

* :ref:`about-docs`
* :ref:`getting-started-docs`

.. _about-docs:

.. toctree::
    :maxdepth: 2
    :caption: About

    about

.. _getting-started-docs:

.. toctree::
    :maxdepth: 2
    :caption: Getting Started

    architecture
    requirements
    installation
    configuration
    usage

.. _faq-docs:

.. toctree::
    :maxdepth: 2
    :caption: FAQ

    faq


##################
License
##################

`becon`_ © 2013-2021 becon GmbH

.. _becon: LICENSE.html
