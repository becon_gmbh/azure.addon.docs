##################
Usage
##################

=========
Sync
=========

.. image:: img/conf_third.png
    :height: 200px
    :alt: azurec sync 
    :align: center

=========
Logs
=========

.. image:: img/conf_four.png
    :height: 200px
    :alt: azurec logs 
    :align: center