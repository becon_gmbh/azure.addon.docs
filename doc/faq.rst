##################
FAQ
##################

**The connection test in i-doit hangs up at "Start test". How do I fix this problem?**

Please unblock/allow access to the file „sync.php“ in „.htaccess“. Use the entry under „jsonrpc.php“ for orientation. You will find this file in the root folder of the i-doit application.


